FROM openjdk:12-alpine
LABEL mantainer="Felipe Pereira <felipe.dspereira@gmail.com>"
ADD target/meeting-scheduler.jar app.jar
COPY ./data /data
RUN apk add --no-cache bash
ENTRYPOINT ["sh", "-c", "java -jar app.jar"]